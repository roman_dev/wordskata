package com.company;


import java.util.List;
import java.util.stream.Stream;

public class Words {

    public long findAllSubWords(List<String> dictionary){
        return dictionary.stream()
                .filter(s -> s.length() == 6)
                .filter(word -> wordIsConcatenated(word, dictionary))
                .count();
    }

    private boolean wordIsConcatenated(String longWord, List<String> dictionary) {
        for (int i = 1; i<6; i++) {
            String firstString = longWord.substring(0, i);
            String secondString = longWord.substring(i, 6);

            Stream<String> words = dictionary.stream()
                    .filter(word -> word.equals(firstString) || word.equals(secondString) );

            if(words.count() == 2) {
                return true;
            }
        }
        return false;
    }
}
