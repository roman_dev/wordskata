package com.company;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class Dictionary {

    public List getDictionary(String fileName) {
        List<String> allWords = new ArrayList<>();
        try {
            Path filePath = Paths.get(getClass().getClassLoader()
                    .getResource(fileName).toURI());
            Stream<String> entries = Files.lines(filePath);
            entries.forEach(allWords::add);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return allWords;
    }
}
