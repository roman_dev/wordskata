package com.company;

import java.util.List;

public class Main {

    public static void main(String[] args) {
        Dictionary dictionary = new Dictionary();
        Words words = new Words();

        List allWords = dictionary.getDictionary("dictionary.txt");
        long concatenatedWords = words.findAllSubWords(allWords);

        System.out.println("Found " + concatenatedWords + " words.");
    }
}
