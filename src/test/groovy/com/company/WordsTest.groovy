package com.company

import spock.lang.Shared
import spock.lang.Specification

class WordsTest extends Specification {

    @Shared
    Words wordsService

    def setupSpec(){
        wordsService = new Words()
    }

    def "Find two concatenated 6-letter words in a dictionary"(){
        given:
        def dictionary = ["advert", "by", "here", "hereby", "or", "tail", "tailor"]

        when:
        long result = wordsService.findAllSubWords(dictionary)

        then:
        result == 2
    }

    def "Word outside the dictionary will not count to result"(){
        given:
        def dictionary = ["trade", "off"]

        when:
        long result = wordsService.findAllSubWords(dictionary)

        then:
        result == 0
    }
}
